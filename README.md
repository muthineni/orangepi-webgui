# OrangePi WebGui

WebGui for Orange Pi and Raspberry Pi. Include WiFi configuration, Modbus setup (RTU and TCP), and MQTT configuration functionalities.